<?php

namespace Post\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Post\Controller\PostController;
use Post\Model\PostRepositoryInterface;

class PostControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new PostController($container->get(PostRepositoryInterface::class));
    }
}