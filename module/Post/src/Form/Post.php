<?php

namespace Post\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;

class Post extends Form
{
    public function __construct($name = null)
    {

        parent::__construct('posts');
        $this->setAttribute('method', 'POST');

        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => 'Title',
            ],
        ]);

        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => 'Title',
            ],
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'text',
            'options' => [
                'label' => 'Description',
            ],
        ]);

        $this->add([
            'name' => 'image_url',
            'type' => 'text',
            'options' => [
                'label' => 'Image URL',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit'
            ],
        ]);

        $file = new Element\File('image');
        $file->setLabel('Post related image');
        $file->setAttribute('id', 'image');

        $this->add($file);
    }
}

