<?php

namespace Post\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Post\Form\Post;
use Post\Form\Post as PostForm;
use Post\Model\Post as PostModel;
use Post\Model\PostTable;

class PostController extends AbstractActionController
{
    private $table;

    public function __construct(PostTable $table) {
        $this->table = $table;
    }

    public function indexAction()
    {
        return new ViewModel([
            'posts' => $this->table->fetchAll()
        ]);
    }

    public function addAction()
    {
        $form = new PostForm();
        $form->get('submit')->setValue('Create');

        $request = $this->getRequest();

        if(! $request->isPost()) {
            return ['form' => $form];
        }

        $post = new PostModel();
        $form->setInputFilter($post->getInputFilter());
        $form->setData(array_merge_recursive( $request->getPost()->toArray(), $request->getFiles()->toArray() ));

        if(! $form->isValid()) {
            return ['form' => $form];
        }

        $post->exchangeArray($form->getData());
        $this->table->save($post);
        return $this->redirect()->toRoute('post');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if($id === 0) {
            return $this->redirect()->toRoute('post', ['action' => 'add']);
        }

        try {
            $postData = $this->table->getPost($id);
            $post = $postData->current();
        } catch( \Exception $e ) {
            return $this->redirect()-> toRoute('post', ['action' => 'index']);
        }

        $form = new PostForm();
        $form->bind($post);
        $form->get('submit')->setAttribute('value', 'Edit Post');

        $request = $this->getRequest();

        $data = [
            'id' => $id,
            'form' => $form
        ];

        if(!$request->isPost()) {
            return $data;
        }

        $form->setInputFilter($post->getInputFilter());
        $form->setData($request->getPost());

        if(!$form->isValid()) {
            return $data;
        }

        $post->exchangeArray((array) $form->getData());
        $this->table->save($post);
        return $this->redirect()->toRoute('post', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if($id === 0) {
            return $this->redirect()->toRoute('post', ['action' => 'index']);
        }

        $request = $this->getRequest();
        if($request->isPost()) {
            $conf = $request->getPost('del', 'No');

            if($conf == 'Yes') {
                $this->table->delete($id);
            }
            return $this->redirect()->toRoute('post', ['action' => 'index']);
        }

        return [
            'id' => $id,
            'album' => $this->table->getPost($id)
        ];
    }
}