<?php

namespace Post\Model;

interface PostRepositoryInterface {

    public function fetchAll();

    public function getPost(int $id);

    public function save(Post $post);

    public function delete(int $id);
}