<?php

namespace Post\Model;

use Laminas\Filter\File\RenameUpload;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\FileInput;
use Laminas\InputFilter\InputFilter;
use Laminas\InputFilter\InputFilterAwareInterface;
use Laminas\InputFilter\InputFilterInterface;
use Laminas\Validator\File\Extension;
use Laminas\Validator\File\Size;
use Laminas\Validator\StringLength;

class Post implements InputFilterAwareInterface {

    public $id;
    public $title;
    public $description;
    public $image_url;
    public $image;
    public $created_on;

    private $inputFilter;

    public function exchangeArray(array $data) {
        $image = $data['id'] ? ( $data['image'] ) : ( $data['image']['tmp_name'] ? substr(strrchr(rtrim($data['image']['tmp_name'], '/'), '/'), 1) : null );
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->title = $data['title'] ?? null;
        $this->description = $data['description'] ?? null;
        $this->image_url = $data['image_url'] ?? null;
        $this->image = $image;
        $this->created_on = $data['created_on'] ?? null;
    }

    public function getArrayCopy(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image_url' => $this->image_url,
            'image' => $this->image,
            'created_on' => $this->created_on
        ];
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \DomainException(sprintf('%s does not allow injection of an alternative input filter', __CLASS__));
    }

    public function getInputFilter()
    {
        if($this->inputFilter) {
            return $this->inputFilter;
        }

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'description',
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'image_url',
            'filters' => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name' => 'image',
            'type' => FileInput::class,
            'filters' => [
                [
                    'name' => RenameUpload::class,
                    'options' => [
                        'use_upload_extension' => true,
                        'randomize' => false,
                        'overwrite' => true,
                        'target' => 'public/img/posts',
                    ],
                ],
            ],
            'validators' => [
                [
                    'name' => Extension::class,
                    'options' => [
                        'extension' => 'png, jpg', 'jpeg',
                        'message' => 'File extension does not match',
                    ],
                ],
                [
                    'name' => Size::class,
                    'options' => [
                        'max' => '4MB',
                        'message' => 'Image size too large',
                    ],
                ],
            ],
        ]);

        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }
}