<?php

namespace Post\Model;

use http\Exception\RuntimeException;
use Laminas\Db\TableGateway\TableGatewayInterface;

class PostTable {
    private $gateway;

    public function __construct(TableGatewayInterface $gateway) {
        $this->gateway = $gateway;
    }

    public function fetchAll() {
        return $this->gateway->select();
    }

    public function getPost(int $id) {
        $post = $this->gateway->select(['id' => $id]);
        if(!$post->current()) {
            throw new RuntimeException(sprintf('Post not found for id %d!', $id));
        }
        return $post;
    }

    public function save(Post $post) {
        $data = [
            'title' => $post->title,
            'image_url' => $post->image_url,
            'description' => $post->description,
            'image' => $post->image,
        ];

        $id = $post->id;
        if(!$id) {
            $this->gateway->insert($data);
            return;
        }
        try {
            $this->getPost($id);
        } catch(RuntimeException $e) {
            throw new RuntimeException(sprintf('Post can\'t be updated for id %d!', $id));
        }
        $this->gateway->update($data, ['id' => $id]);
    }

    public function delete(int $id) {
        $this->gateway->delete(['id' => $id]);
    }
}