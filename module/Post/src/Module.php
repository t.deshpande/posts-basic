<?php
namespace Post;

use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Post\Controller\PostController;
use Post\Model\Post;
use Post\Model\PostTable;

class Module implements ConfigProviderInterface {

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getControllerConfig() {
        return [
            'factories' => [
                PostController::class => function($container) {
                return new PostController($container->get(PostTable::class));
                }
            ]
        ];
    }

    public function getServiceConfig() {
        return [
            'factories' => [
                PostTable::class => function($container) {
                    $gateway = $container->get(Model\PostTableGateway::class);
                    return new PostTable($gateway);
                },
                Model\PostTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSet = new ResultSet();
                    $resultSet->setArrayObjectPrototype(new Post());
                    return new TableGateway('posts', $dbAdapter, null, $resultSet);
                }
            ],
        ];
    }
}